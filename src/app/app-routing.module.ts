import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EerComponent } from './eer/eer.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'eer', component: EerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
