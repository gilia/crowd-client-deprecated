import { Component, OnInit } from '@angular/core';

declare var CrowdEER;

@Component({
  selector: 'app-eer',
  templateUrl: './eer.component.html',
  styleUrls: ['./eer.component.scss']
})
export class EerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    var eer = new CrowdEER({
      selector: 'editor',
      palette: {
        grid: {
          size: 90,
          columns: 2
        }
      }
    });
  }

}
