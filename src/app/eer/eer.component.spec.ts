import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EerComponent } from './eer.component';

describe('EerComponent', () => {
  let component: EerComponent;
  let fixture: ComponentFixture<EerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
